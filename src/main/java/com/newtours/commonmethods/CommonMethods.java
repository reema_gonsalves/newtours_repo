package com.newtours.commonmethods;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

public class CommonMethods {
	
	//This method is used to capture screenshot
	public static void captureScreenShot(WebDriver driver, String folder, String fileName) throws IOException {
		
		//Take a screenshot as a file format
		File screenshotFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		
		try {
			//copy the file to the desired location using the copyFile method
			FileUtils.copyFile(screenshotFile, new 
					File(folder + "/"+ fileName +".png"));
		}catch(IOException ie) {
			System.out.println(ie.getMessage());
		}
	}
}
