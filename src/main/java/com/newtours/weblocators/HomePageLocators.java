package com.newtours.weblocators;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class HomePageLocators {

	/* ArrivingIn_NAME
	ReturningMonth_NAME
	ReturningDate_NAME
	EconomyClass_CSSSELECTOR
	BusinessClass_CSSSELECTOR
	FirstClass_CSSSELECTOR
	String Airline_NAME
	Continue_NAME
	Continue1_NAME
	FirstName_NAME
	LastName_NAME
	Number_NAME
	Securepurchase_NAME
	String CofirmMessage_XPATH*/

	public static final String RoundTrip_XPATH = "html/body/div[1]/table/tbody/tr/td[2]/table/tbody/tr[4]/td/table/tbody/tr/td[2]/table/tbody/tr[5]/td/form/table/tbody/tr[2]/td[2]/b/font/input[1]";
	public static final String Oneway_XPATH = "html/body/div[1]/table/tbody/tr/td[2]/table/tbody/tr[4]/td/table/tbody/tr/td[2]/table/tbody/tr[5]/td/form/table/tbody/tr[2]/td[2]/b/font/input[2]";
	public static final String Passengers_NAME = "passCount";
	public static final String Departingfrom_NAME = "fromPort";
	public static final String OnMonth_NAME = "fromMonth";
	public static final String OnDate_NAME = "fromDay";
	public static final String ArrivingIn_NAME = "toPort";
	public static final String ReturningMonth_NAME = "toMonth";
	public static final String ReturningDate_NAME = "toDay";
	public static final String EconomyClass_CSSSELECTOR = "input[value=Coach]";
	public static final String BusinessClass_CSSSELECTOR = "input[value=Business]";
	public static final String FirstClass_CSSSELECTOR = "input[value=First]";
	public static final String Airline_NAME = "airline";
	public static final String Continue_NAME = "findFlights";
	public static final String Continue1_NAME = "reserveFlights";
	public static final String FirstName_NAME = "passFirst0";
	public static final String LastName_NAME = "passLast0";
	public static final String Number_NAME = "creditnumber";
	public static final String Securepurchase_NAME = "buyFlights";
	public static final String CofirmMessage_XPATH = "html/body/div[1]/table/tbody/tr/td[2]/table/tbody/tr[4]/td/table/tbody/tr[1]/td[2]/table/tbody/tr[3]/td/p/font/b/font[2]";

	private static WebElement element = null;

	// radioButton_RoundTrip
	public static WebElement rdo_RoundTrip(WebDriver driver){
		element = driver.findElement(By.xpath(RoundTrip_XPATH));
		return element;
	}

	// RadioBtn_Oneway_XPATH
	public static WebElement rdo_Oneway(WebDriver driver){
		element = driver.findElement(By.xpath(Oneway_XPATH));
		return element;
	}

	// lst_Passengers
	public static WebElement lst_Passengers(WebDriver driver){
		element = driver.findElement(By.name(Passengers_NAME));
		return element;
	}

	// list_Departingfrom
	public static WebElement lst_Departingfrom(WebDriver driver){
		element = driver.findElement(By.name(Departingfrom_NAME));
		return element;
	}

	// list_OnMonth
	public static WebElement lst_OnMonth(WebDriver driver){
		element = driver.findElement(By.name(OnMonth_NAME));
		return element;
	}

	// list_OnDate
	public static WebElement lst_OnDate(WebDriver driver){
		element = driver.findElement(By.name(OnDate_NAME));
		return element;
	}

	// lst_Arriving In
	public static WebElement lst_ArrivingIn(WebDriver driver){
		element = driver.findElement(By.name(ArrivingIn_NAME));
		return element;
	}

	// list_returnMonth
	public static WebElement lst_retrunMonth(WebDriver driver){
		element = driver.findElement(By.name(ReturningMonth_NAME));
		return element;
	}

	// list_returnDate
	public static WebElement lst_returnDate(WebDriver driver){
		element = driver.findElement(By.name(ReturningDate_NAME));
		return element;
	}

	// radioButton_BusinessClass
	public static WebElement rdo_BusinessClass(WebDriver driver){
		element = driver.findElement(By.cssSelector(BusinessClass_CSSSELECTOR));
		return element;
	}

	// radioButton_EconomyClass
	public static WebElement rdo_EconomyClass(WebDriver driver){
		element = driver.findElement(By.cssSelector(EconomyClass_CSSSELECTOR));
		return element;
	}

	// radioButton_FirstClass
	public static WebElement rdo_FirstClass(WebDriver driver){
		element = driver.findElement(By.cssSelector(FirstClass_CSSSELECTOR));
		return element;
	}

	// lst_Airline name
	public static WebElement lst_Airline(WebDriver driver){
		element = driver.findElement(By.name(Airline_NAME));
		return element;
	}

	// btn_Continue
	public static WebElement btn_Continue(WebDriver driver){
		element = driver.findElement(By.name(Continue_NAME));
		return element;
	}

	//btn_Continue1
	public static WebElement btn_Continue1(WebDriver driver){
		element = driver.findElement(By.name(Continue1_NAME));
		return element;
	}

	// txt_FirstName
	public static WebElement txt_FirstName(WebDriver driver){
		element = driver.findElement(By.name(FirstName_NAME));
		return element;
	}

	// txt_LastName
	public static WebElement txt_LastName(WebDriver driver){
		element = driver.findElement(By.name(LastName_NAME));
		return element;
	}

	// txt_LastName
	public static WebElement txt_CreditNumber(WebDriver driver){
		element = driver.findElement(By.name(Number_NAME));
		return element;
	}

	// btn_SecurePurchase
	public static WebElement btn_Purchase(WebDriver driver){
		element = driver.findElement(By.name(Securepurchase_NAME));
		return element;
	}

}
