package com.newtours.weblocators;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class LoginLocators {
	
	private static WebElement element = null;

	public static final String user_name = "userName";
	public static final String pass_word = "password";
	public static final String sign_in = "login";

	//Get the user name element
	public static WebElement txt_userName(WebDriver driver) {
		element = driver.findElement(By.name(user_name));
		return element;
	}

	//Get the password element
	public static WebElement txt_password(WebDriver driver)	{
		element = driver.findElement(By.name(pass_word));
		return element;
	}

	//Get the login button
	public static WebElement btn_SignIn(WebDriver driver){
		element = driver.findElement(By.name(sign_in));
		return element;
	}

}
