package com.newtours.excelutilities;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelUtilMethods {

	static XSSFWorkbook workbook; // POI reference to workbook
	static XSSFWorkbook existingBook = null;
	static XSSFSheet sheet; //POI reference to work sheet
	static String resultData ="";
	static FileInputStream inputStream=null;
	static FileOutputStream outputStream= null;

	//To read data from the excel sheet with String sheet name
	public static String getData(String filePath, String sheetName, int rowNo, int colNo) throws IOException{
		System.out.println("Inside getData with Sheet name method");
		try {
			File testDataFile = new File(filePath);
			inputStream = new FileInputStream(testDataFile);
			workbook = new XSSFWorkbook(inputStream);
			sheet = workbook.getSheet(sheetName);
			resultData = sheet.getRow(rowNo).getCell(colNo).getStringCellValue();
			System.out.println("The result data="+resultData);
		}finally {
			inputStream.close();
		}
		return resultData;		
	}

	//To read data from the excel sheet with int sheet number
	public static String getData(String filePath, int sheetNo, int rowNo, int colNo) 
			throws IOException{
		try {
			System.out.println("Inside getData with Sheet name method");
			File testDataFile = new File(filePath);
			inputStream = new FileInputStream(testDataFile);
			workbook = new XSSFWorkbook(inputStream);
			sheet = workbook.getSheetAt(sheetNo);
			resultData = sheet.getRow(rowNo).getCell(colNo).getStringCellValue();
			System.out.println("The result data="+resultData);
		}finally {
			inputStream.close();
		}
		return resultData;		
	}

	//Write test results to the excel file
	public static void writeResult(String filePath, int sheetNo, int rowNo, int colNo, 
			String value) throws IOException{
		try {
			File resultFile = new File(filePath);
			inputStream = new FileInputStream(resultFile);
			workbook = new XSSFWorkbook(inputStream);
			sheet = workbook.getSheetAt(sheetNo);
			
			//Create row only once so data is not overridden for previous content
			if(sheet.getRow(rowNo) == null) {
				System.out.println("Creating row");
				sheet.createRow(rowNo);
			}
			sheet.getRow(rowNo).createCell(colNo).setCellValue(value);
			System.out.println("Result written to the file");
			
			outputStream = new FileOutputStream(new File(filePath));
			workbook.write(outputStream);

		}catch(Exception e){
			e.printStackTrace();
		}finally {
			inputStream.close();
			outputStream.close();
		}		
	}

	//To get the row count in a sheet
	public static int getRowCount(int sheetNo) {
		System.out.println("Inside Row count method");
		int rowCount = workbook.getSheetAt(sheetNo).getLastRowNum();
		return rowCount;
	}
}
