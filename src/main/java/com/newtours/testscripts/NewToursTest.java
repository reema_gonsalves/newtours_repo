package com.newtours.testscripts;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.newtours.commonmethods.CommonMethods;
import com.newtours.excelutilities.ExcelUtilMethods;
import com.newtours.weblocators.HomePageLocators;
import com.newtours.weblocators.LoginLocators;

import io.github.bonigarcia.wdm.WebDriverManager;


public class NewToursTest {

	WebDriver driver = null;

	String testDataFilePath = "src/test/java/TestData.xlsx";
	String resultsFilePath = "src/test/java/Results.xlsx";
	String screenshotsFolder = "Test_Screenshots";
	String sheetName="";	

	//Login to specified URL
	@Test(priority=1) 
	public void login(){
		try {	
			System.out.println("Test Priority 1");
			//Open the app URL
			sheetName = "URL";
			driver.get(ExcelUtilMethods.getData(testDataFilePath, sheetName, 1, 0));
			System.out.println("This is Login Page");

			//Screenshot of Login screen
			CommonMethods.captureScreenShot(driver, screenshotsFolder, "Login_Screen");

			//Result data for Login Page on Sheet 2 onwards
			ExcelUtilMethods.writeResult(resultsFilePath, 1, 1, 0, "Test1");
			ExcelUtilMethods.writeResult(resultsFilePath, 1, 1, 1, "Login Page displayed");
			ExcelUtilMethods.writeResult(resultsFilePath, 1, 1, 2, "Passed");

			//Pass User name and password reading from the TestData file
			LoginLocators.txt_userName(driver).sendKeys(
					ExcelUtilMethods.getData(testDataFilePath, sheetName, 1, 1));
			System.out.println("User name entered");
			LoginLocators.txt_password(driver).sendKeys(
					ExcelUtilMethods.getData(testDataFilePath, sheetName, 1, 2));
			System.out.println("Password entered");

			CommonMethods.captureScreenShot(driver, screenshotsFolder, "Username_Password_Entered");

			LoginLocators.btn_SignIn(driver).click();

			CommonMethods.captureScreenShot(driver, screenshotsFolder, "Login_Button_Clicked");

			//Result data for Login Button Clicked
			ExcelUtilMethods.writeResult(resultsFilePath, 1, 2, 0, "Test2");
			ExcelUtilMethods.writeResult(resultsFilePath, 1, 2, 1, "Login Button Clicked");
			ExcelUtilMethods.writeResult(resultsFilePath, 1, 2, 2, "Passed");

			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		}catch(Exception e) {
			e.printStackTrace();
		}
	}

	//Booking the Flight
	@Test(priority=2) 
	public void bookFlight() throws IOException {
		System.out.println("Test Priority 2");
		
		//Capture screen shot
		CommonMethods.captureScreenShot(driver, screenshotsFolder, "Home_Page_Displayed");

		//Write Test data
		//Result data for Login Page
		ExcelUtilMethods.writeResult(resultsFilePath, 1, 3, 0, "Test3");
		ExcelUtilMethods.writeResult(resultsFilePath, 1, 3, 1, "Home Page displayed");
		ExcelUtilMethods.writeResult(resultsFilePath, 1, 3, 2, "Passed");

		//Start getting elements and sending data for booking the flight
		//Select the trip type
		String tripType = ExcelUtilMethods.getData(testDataFilePath, 1, 1, 0);
		if(tripType.contentEquals("Round Trip")) {
			HomePageLocators.rdo_RoundTrip(driver).click();
		}else {
			HomePageLocators.rdo_Oneway(driver).click();
		}
		System.out.println("trip type selected");

		//Selecting the passengers count
		Select passCount = new Select(HomePageLocators.lst_Passengers(driver));
		passCount.selectByVisibleText(ExcelUtilMethods.getData(testDataFilePath, 1, 1, 1));

		//Select Departing From value
		Select departFrom = new Select(HomePageLocators.lst_Departingfrom(driver));
		departFrom.selectByVisibleText(ExcelUtilMethods.getData(testDataFilePath, 1, 1, 2));

		//Select On month value
		Select onMonth = new Select(HomePageLocators.lst_OnMonth(driver));
		onMonth.selectByVisibleText(ExcelUtilMethods.getData(testDataFilePath, 1, 1, 3));

		//Select On day value
		Select onDay = new Select(HomePageLocators.lst_OnDate(driver));
		onDay.selectByVisibleText(ExcelUtilMethods.getData(testDataFilePath, 1, 1, 4));

		//Select Arriving In value
		Select arriveIn = new Select(HomePageLocators.lst_ArrivingIn(driver));
		arriveIn.selectByVisibleText(ExcelUtilMethods.getData(testDataFilePath, 1, 1, 5));

		//Select Returning month value
		Select returnMonth = new Select(HomePageLocators.lst_retrunMonth(driver));
		returnMonth.selectByVisibleText(ExcelUtilMethods.getData(testDataFilePath, 1, 1, 6));

		//Select Returning day value
		Select returnDay = new Select(HomePageLocators.lst_returnDate(driver));
		returnDay.selectByVisibleText(ExcelUtilMethods.getData(testDataFilePath, 1, 1, 7));

		//Select the Service class
		String serviceClass = ExcelUtilMethods.getData(testDataFilePath, 1, 1, 8);
		if(serviceClass.contentEquals("Business Class")) {
			HomePageLocators.rdo_BusinessClass(driver).click();
		}else if(serviceClass.contentEquals("Economy Class")){
			HomePageLocators.rdo_EconomyClass(driver).click();
		}else {
			HomePageLocators.rdo_FirstClass(driver).click();
		}
		System.out.println("service class selected");

		//Select Airline
		Select airline = new Select(HomePageLocators.lst_Airline(driver));
		airline.selectByVisibleText(ExcelUtilMethods.getData(testDataFilePath, 1, 1, 9));

		//Capture screen shot with details filled
		CommonMethods.captureScreenShot(driver, screenshotsFolder, "booking_details_filled");

		//Write Test data
		//Result data for Booking details filled
		ExcelUtilMethods.writeResult(resultsFilePath, 1, 4, 0, "Test4");
		ExcelUtilMethods.writeResult(resultsFilePath, 1, 4, 1, "Booking details filled");
		ExcelUtilMethods.writeResult(resultsFilePath, 1, 4, 2, "Passed");

		//Click the continue btn
		HomePageLocators.btn_Continue(driver).click();

		//Capture screenshot of the next page after continue
		CommonMethods.captureScreenShot(driver, screenshotsFolder, "select_flight_filled");

		//Write Test data
		//Result data for Select Flight filled
		ExcelUtilMethods.writeResult(resultsFilePath, 1, 5, 0, "Test5");
		ExcelUtilMethods.writeResult(resultsFilePath, 1, 5, 1, "Select Flight details filled");
		ExcelUtilMethods.writeResult(resultsFilePath, 1, 5, 2, "Passed");

		//Click on the second continue button
		HomePageLocators.btn_Continue1(driver).click();

		//Enter the First Name
		HomePageLocators.txt_FirstName(driver).sendKeys(ExcelUtilMethods.getData(testDataFilePath, 1, 1, 10));

		//Enter the Last Name
		HomePageLocators.txt_LastName(driver).sendKeys(ExcelUtilMethods.getData(testDataFilePath, 1, 1, 11));

		//Enter the Credit Number
		HomePageLocators.txt_CreditNumber(driver).sendKeys(ExcelUtilMethods.getData(testDataFilePath, 1, 1, 12));

		//Capture screenshot of the next page after continue
		CommonMethods.captureScreenShot(driver, screenshotsFolder, "personal_details");

		//Write Test data
		//Result data for Select Flight filled
		ExcelUtilMethods.writeResult(resultsFilePath, 1, 6, 0, "Test6");
		ExcelUtilMethods.writeResult(resultsFilePath, 1, 6, 1, "Personal details filled");
		ExcelUtilMethods.writeResult(resultsFilePath, 1, 6, 2, "Passed");
		
		//Click on button Secure Purchase
		HomePageLocators.btn_Purchase(driver).click();
	}
	
	//Check for confirmation message
	@Test(priority = 3)
	public void confirmMessageCheck() throws IOException {
		
		System.out.println("Test Priority 3");
		
		String confirmTitle="Flight Confirmation: Mercury Tours";
		boolean flag = false;
		if(driver.getTitle().equalsIgnoreCase(confirmTitle)){
			flag = true;
			System.out.println("Flight booked successfully");
			
			//Capture screenshot of the next page after continue
			CommonMethods.captureScreenShot(driver, screenshotsFolder, "Booking_success");
			
			//Write Test data
			//Result data for Select Flight filled
			ExcelUtilMethods.writeResult(resultsFilePath, 1, 7, 0, "Test7");
			ExcelUtilMethods.writeResult(resultsFilePath, 1, 7, 1, "Flight booking successful");
			ExcelUtilMethods.writeResult(resultsFilePath, 1, 7, 2, "Passed");			
		}
		Assert.assertTrue(flag,"Page tile not matching the expected title");		
	}

	@BeforeTest
	public void beforeTest() throws IOException{
		
		System.out.println("Before Test");

		//Create directory for the screen shots to be saved
		File screenShotsDir = new File("Test_Screenshots");
		screenShotsDir.mkdir();

		//Initialize the Browser
		//WebDriverManager.firefoxdriver().setup();
		WebDriverManager.chromedriver().setup();
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		System.out.println("Browser Initialized");

		//Result Data for Browser Initiation
		ExcelUtilMethods.writeResult(resultsFilePath, 0, 1, 0, "Test1");
		ExcelUtilMethods.writeResult(resultsFilePath, 0, 1, 1, "Browser Initialized");
		ExcelUtilMethods.writeResult(resultsFilePath, 0, 1, 2, "Passed");
		System.out.println("Data written to file");

		CommonMethods.captureScreenShot(driver, screenshotsFolder, "Browser_Initiation");

		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}

	@AfterTest
	public void afterTest() {
		System.out.println("After Test");
		driver.close();
	}
}
